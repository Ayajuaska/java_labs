package com.rng.Lab2;

public interface Present extends Product {
    Boolean itCanBePresented();
}
